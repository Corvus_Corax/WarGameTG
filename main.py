import asyncio
from SQLRequests.SQLR import Sqlrequests
from TOKEN import TOKEN
from keyinline import BOARDS
from telebot import types
from telebot.async_telebot import AsyncTeleBot


Sqlr = Sqlrequests(False)
bot = AsyncTeleBot(TOKEN)

menu = types.ReplyKeyboardMarkup(True)
menu.row("/base", "/inv", "/shop")
menu.row("/acc", "/attack", "/help")


selected_keyboard = 0


@bot.message_handler(commands=['start'])
async def start_message(message):
    global selected_keyboard
    selected_keyboard = 0
    if Sqlr.user(member_id=message.from_user.id):
        await bot.send_message(message.chat.id, "С Возвращением на объект 261!!!", reply_markup=gen_markup())
    else:
        st_kb = types.ReplyKeyboardMarkup(True)
        st_kb.row("/help", "/reg")
        await bot.send_message(message.chat.id, "Storm War Games\nAmator 2.0 server\nОписание: Подозрительные шашечки", reply_markup=gen_markup())


@bot.message_handler(regexp='help')
async def game_help(message):
    await bot.send_message(message.chat.id, "Er404 Мы это где-то проебали :D")


@bot.message_handler(regexp='reg')
async def reg_user(message):
    try:
        Sqlr.new_player(message.from_user)
        await bot.send_message(message.chat.id, "Добро пожаловать на объект 261", reply_markup=gen_markup())
    except Exception as ex:
        print(ex)


@bot.message_handler(regexp='test')
async def test_feature(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add('Text')
    await bot.send_message(message.chat.id, 'Text', reply_markup=markup)

def gen_markup():
    markup = types.InlineKeyboardMarkup()
    for i in BOARDS[selected_keyboard]:
        markup.row(*i)
    return markup

@bot.callback_query_handler(func=lambda call: "/start")
async def callback_query(call):
    global selected_keyboard
    if call.data == "btn_inv":
        selected_keyboard = 1
        await bot.send_message(call.message.chat.id, "Ты чево? Думал у тебя что-то есть? Ладно, есть.", reply_markup=gen_markup())

    elif call.data == "btn_base":
        selected_keyboard = 0
        await bot.send_message(call.message.chat.id, "Это база",reply_markup=gen_markup())



asyncio.run(bot.polling())