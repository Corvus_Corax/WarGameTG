import PIL
from PIL import Image, ImageDraw, ImageColor

class Render:
    
    unit_pos = [[(123 * i, 123 * j) for i in range(5)] for j in range(5)]

    @staticmethod
    def draw_grid(color: str = None):
        
        if color is None:
            color = 'green'

        img = Image.new(mode="RGB", size=(615, 615), color='black')

        image = ImageDraw.Draw(img)

        
        for i in range(1, 5):
            x, y = 123 * i, 123 * i
            image.line((x-1, 0, x-1, 615), width=4, fill=ImageColor.getrgb(color))
            image.line((0, y-1, 615, y-1), width=4, fill=ImageColor.getrgb(color))

        return img

    @staticmethod
    def render_unit(img, pos, unit: str):
        img.paste(Image.open("./assets/"+unit+".png"), (pos[0],pos[1]))
        return img